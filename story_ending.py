#!/usr/bin/python3

import re
import os.path
import numpy as np
import pandas as pd

from gensim.models import Word2Vec
from gensim.utils import simple_preprocess

from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics import classification_report, accuracy_score, confusion_matrix

from ast import literal_eval
from nltk import word_tokenize, pos_tag


def read_files():
	"""Read CSV files"""
	train_2016 = pd.read_csv("Data/2016_val.csv", delimiter=",")
	test_2016 = pd.read_csv("Data/2016_test.csv", delimiter=",")
	test_2018 = pd.read_csv("Data/2018_val.csv", delimiter=",") 
	test_20181 = pd.read_csv("Data/2018_test.csv", delimiter=",") 

	return train_2016, test_2016, test_2018, test_20181


def separate(file):
	"""Separate one row with two endings into two rows, each with a different ending.
	Add a AnswerThisEnding column to the dataframe"""
	# Combine both ending sentences into one column
	file['EndingSentence'] = file['RandomFifthSentenceQuiz1'] + " :: " + file['RandomFifthSentenceQuiz2'] # SettingWithCopyWarning ????

	# Transform list of answers in list of 0's and 1's
	answers = file['AnswerRightEnding'].values.tolist()
	answers_l = [[0, 1] if x == 2 else [1, 0] for x in answers]
	answers_final = list(np.array(answers_l).flat)

	# Split the ending sentences into separate rows
	df = split_sentences(file)

	# Add 0 (wrong ending) or 1 (right ending) to the AnswerThisEnding column for every ending
	df['AnswerThisEnding'] = answers_final

	return df


def split_sentences(file):
	"""Split the column 'EndingSentence', so every ending is on a separate row"""
	ending = file["EndingSentence"].str.split(' :: ', expand=True).stack()
	ind = ending.index.get_level_values(0)
	df = file.loc[ind].copy()
	df["EndingSentence"] = ending.values

	return df


#### STYLISTIC FEATURES

def all_ngrams(train):
	"""Get all the character 4-grams and word 1- to 5-grams from the story endings"""
	# Remove non-numerical characters
	file = remove_punct_end(train)
	index = file.index

	# Create character 4-grams with a minimum frequency of 5
	vec, matrix, features_names = create_character_grams(file)

	# Replace nouns, verbs, adjectives and adverbs with their postag, create word n-grams
	file = replace_pos(file)
	vec2, matrix2, features_names2 = create_word_grams(file)

	return vec, vec2, matrix, matrix2, features_names, features_names2, index


def test_cgrams(test, vec):
	"""For test file, get all character 4-grams"""
	file = remove_punct_end(test)
	index = file.index
	test_matrix = vec.transform(file['EndingSentence'])

	return test_matrix, index


def test_wgrams(test, vec2):
	"""For test file, get all word 1- to 5-grams"""
	file = remove_punct_end(test)
	file = replace_pos(test)
	test_matrix2 = vec2.transform(file['EndingSentence'])

	return test_matrix2


def remove_punct_end(file):
	"""Remove all non-alphanumerical characters from the endings"""
	file['EndingSentence'] = file['EndingSentence'].replace('[^a-zA-Z0-9 ]', '', regex=True)

	return file


def replace_pos(file):
	"""Replace nouns, verbs, adjectives and adverbs with their part-of-speech tag"""
	file['EndingSentence'] = file['EndingSentence'].apply(word_tokenize).apply(pos_tag, tagset='universal').apply(get_pos_tag)

	return file


def create_character_grams(file):
	"""Create character 4-grams for all the endings"""
	vec = CountVectorizer(analyzer='char', ngram_range = (4, 4), min_df=5)
	matrix = vec.fit_transform(file['EndingSentence'])
	features_names = vec.get_feature_names()

	return vec, matrix, features_names	


def get_pos_tag(i):
	"""For every word, part-of-speech tag pair in a sentence, replace certain words
	with their part-of-speech tag and return sentence"""
	lst = []
	pos = ['NOUN', 'VERB', 'ADJ', 'ADV']

	for w, p in i:
		if p in pos:
			lst.append(p)
		else:
			lst.append(w)

	return ' '.join(lst)


def create_word_grams(file):
	"""Create word 1- to 5-grams for all the endings"""
	vec2 = CountVectorizer(analyzer='word', ngram_range = (1, 5), min_df=5, binary=True)
	matrix2 = vec2.fit_transform(file['EndingSentence'])
	features_names2 = vec2.get_feature_names()

	return vec2, matrix2, features_names2


def sentence_length(file):
	"""Get sentence length for all endings"""
	file['SentenceLength'] = file['EndingSentence'].apply(lambda x: x.split()).apply(len)

	return file


def create_df(matrix, matrix2, features_names, features_names2, index):
	"""Create dataframes containing all n-gram features"""
	char_df = pd.DataFrame(matrix.toarray(), columns=np.array(features_names), index=index)
	word_df = pd.DataFrame(matrix2.toarray(), columns=np.array(features_names2), index=index)

	df = pd.concat([char_df, word_df], axis=1)

	return df


def stylistic_features(train_sep, test_sep, test_2016_sep, test_2018_sep, test_20181_sep):
	"""Get all stylistic features in a csv file"""
	filelist = ['Dataframes/train_styl.csv', 'Dataframes/test_styl.csv', 'Dataframes/test_2016_styl.csv', 'Dataframes/test_2018_styl.csv', 'Dataframes/test_20181_styl.csv']

	while True:
		liststyl = []

		for file in filelist:
			liststyl.append(os.path.isfile(file))

		if all(liststyl):
			print("All stylistic files exist")
			break
		else:
			# Get sentence length for every ending
			train_sep = sentence_length(train_sep)
			test_sep = sentence_length(test_sep)
			test_2016_sep = sentence_length(test_2016_sep)
			test_2018_sep = sentence_length(test_2018_sep)
			test_20181_sep = sentence_length(test_20181_sep)

			# Get all character and word n-grams for training set
			vec, vec2, matrix, matrix2, features_names, features_names2, index = all_ngrams(train_sep)

			# Get all character 4-grams for test sets
			test_char_matrix, index_test = test_cgrams(test_sep, vec)
			test_2016_char_matrix, index_test_2016 = test_cgrams(test_2016_sep, vec)
			test_2018_char_matrix, index_test_2018 = test_cgrams(test_2018_sep, vec)
			test_20181_char_matrix, index_test_20181 = test_cgrams(test_20181_sep, vec)

			# Get all word 1- to 5-grams for test sets
			test_word_matrix = test_wgrams(test_sep, vec2)
			test_2016_word_matrix = test_wgrams(test_2016_sep, vec2)
			test_2018_word_matrix = test_wgrams(test_2018_sep, vec2)
			test_20181_word_matrix = test_wgrams(test_20181_sep, vec2)

			# Create dataframes of all the n-grams
			train_gram_df = create_df(matrix, matrix2, features_names, features_names2, index)
			test_gram_df = create_df(test_char_matrix, test_word_matrix, features_names, features_names2, index_test)
			test_2016_gram_df = create_df(test_2016_char_matrix, test_2016_word_matrix, features_names, features_names2, index_test_2016)
			test_2018_gram_df = create_df(test_2018_char_matrix, test_2018_word_matrix, features_names, features_names2, index_test_2018)
			test_20181_gram_df = create_df(test_20181_char_matrix, test_20181_word_matrix, features_names, features_names2, index_test_20181)

			# Create 1 dataframe per dataset with all stylistic features
			train_stylistic = pd.concat([train_sep['SentenceLength'], train_gram_df], axis=1)
			test_stylistic = pd.concat([test_sep['SentenceLength'], test_gram_df], axis=1)
			test_2016_stylistic = pd.concat([test_2016_sep['SentenceLength'], test_2016_gram_df], axis=1)
			test_2018_stylistic = pd.concat([test_2018_sep['SentenceLength'], test_2018_gram_df], axis=1)
			test_20181_stylistic = pd.concat([test_20181_sep['SentenceLength'], test_20181_gram_df], axis=1)

			# Create csv files for all the stylistic features dataframes
			train_stylistic.to_csv(filelist[0], encoding='utf-8', index=False)
			test_stylistic.to_csv(filelist[1], encoding='utf-8', index=False)
			test_2016_stylistic.to_csv(filelist[2], encoding='utf-8', index=False)
			test_2018_stylistic.to_csv(filelist[3], encoding='utf-8', index=False)
			test_20181_stylistic.to_csv(filelist[4], encoding='utf-8', index=False)

			print("All stylistic files created")


#### SIMILARITY FEATURES

def create_story(df):
	"""Combine the 4 input sentence into one story and tokenize story and ending"""
	df['Story'] = df['InputSentence1'] + " " + df['InputSentence2'] + " " + df['InputSentence3'] + " " + df['InputSentence4']
	df['Story'] = df['Story'].apply(simple_preprocess)
	df['EndingSentence'] = df['EndingSentence'].apply(simple_preprocess)

	return df


def get_vector(i):
	"""For every word in the sentence, get the vector"""
	# Word2Vec model trained using ROCStories
	b = []
	w2v_model = Word2Vec.load('word2vec')
	lst = [w2v_model.wv[w] for w in i if w in w2v_model.wv.vocab]
	
	for a in lst:
		b.append(a.tolist())
	
	return b


def vectors(train_sep, test_sep, test_2016_sep, test_2018_sep, test_20181_sep):
	"""Get all similarity features in a dataframe"""
	
	# Combine first 4 sentence into 1 story and tokenize story and ending
	train = create_story(train_sep)
	test = create_story(test_sep)
	test_2016 = create_story(test_2016_sep)
	test_2018 = create_story(test_2018_sep)
	test_20181 = create_story(test_20181_sep)

	print("Get story vectors...")
	# Get vector of every word in story and create new column
	train['StoryVec'] = train['Story'].apply(get_vector)
	test['StoryVec'] = test['Story'].apply(get_vector)
	test_2016['StoryVec'] = test_2016['Story'].apply(get_vector)
	test_2018['StoryVec'] = test_2018['Story'].apply(get_vector)
	test_20181['StoryVec'] = test_20181['Story'].apply(get_vector)

	print("Get ending vectors...")
	# Get vector of every word in ending and create new column
	train['EndingVec'] = train['EndingSentence'].apply(get_vector)
	test['EndingVec'] = test['EndingSentence'].apply(get_vector)
	test_2016['EndingVec'] = test_2016['EndingSentence'].apply(get_vector)
	test_2018['EndingVec'] = test_2018['EndingSentence'].apply(get_vector)
	test_20181['EndingVec'] = test_20181['EndingSentence'].apply(get_vector)

	print("Create new file...")
	# Create csv files for all the similarity features dataframes
	train.to_csv('Dataframes/train_sim.csv', encoding='utf-8', index=False)
	test.to_csv('Dataframes/test_sim.csv', encoding='utf-8', index=False)
	test_2016.to_csv('Dataframes/test_2016_sim.csv', encoding='utf-8', index=False)
	test_2018.to_csv('Dataframes/test_2018_sim.csv', encoding='utf-8', index=False)
	test_20181.to_csv('Dataframes/test_20181_sim.csv', encoding='utf-8', index=False)


def similarity():
	"""Calculate averages and similarity scores vectors"""
	df = pd.read_csv("Dataframes/test_20181_sim.csv", delimiter=",", converters={'StoryVec':literal_eval, 'EndingVec':literal_eval})
	cols = ['InputStoryid','InputSentence1','InputSentence2','InputSentence3','InputSentence4','RandomFifthSentenceQuiz1','RandomFifthSentenceQuiz2','AnswerRightEnding','EndingSentence','AnswerThisEnding','Story']
	df.drop(cols, inplace=True, axis=1)

	print("Calculate length, sum, average...")

	# Get length, sum and average of vectors
	df['StoryLength'] = df['StoryVec'].apply(len)
	df['EndingLength'] = df['EndingVec'].apply(len)
	df['StorySum'] = df['StoryVec'].apply(get_sum)
	df['EndingSum'] = df['EndingVec'].apply(get_sum)
	df['StoryAvg'] = df.apply(lambda x: get_average(x['StorySum'], x['StoryLength']), axis=1)
	df['EndingAvg'] = df.apply(lambda x: get_average(x['EndingSum'], x['EndingLength']), axis=1)

	print("Calculate cosine similarity...")
	# Calculate cosine similarity between stories and endings
	df['CosSim'] = df.apply(lambda x: get_cosine(x['StoryAvg'], x['EndingAvg']), axis=1)

	print("Create new file...")
	df['CosSim'].to_csv('Dataframes/test_20181_cos.csv', encoding='utf-8', index=False, header=True)


def get_sum(i):
	"""Add all 100 vectors into one vector"""
	return [sum(j) for j in zip(*i)]


def get_average(sumv, length):
	"""Get average of the vectors"""
	average = [s/length for s in sumv]

	return average


def get_cosine(story, ending):
	"""Get the cosine similarity between the stories and endings"""
	sim = cosine_similarity([story], [ending])

	return sim


#### BERT

def total_story(df):
	"""Combine the 4 input sentence with the ending to form a story"""
	df['Story'] = df['InputSentence1'] + " " + df['InputSentence2'] + " " + df['InputSentence3'] + " " + df['InputSentence4'] + " " + df['EndingSentence']
	df = df.filter(['Story'])

	return df


def create_file_bert(train_sep, test_sep, test_2016_sep, test_2018_sep, test_20181_sep):
	"""For every data set create a file with just the story to use for BERT"""
	# Get just the story as dataframe
	train_story = total_story(train_sep)
	test_story = total_story(test_sep)
	test_2016_story = total_story(test_2016_sep)
	test_2018_story = total_story(test_2018_sep)
	test_20181_story = total_story(test_20181_sep)

	# Create CSV files
	train_story.to_csv('Dataframes/train_story.csv', encoding='utf-8', index=False)
	test_story.to_csv('Dataframes/test_story.csv', encoding='utf-8', index=False)
	test_2016_story.to_csv('Dataframes/test_2016_story.csv', encoding='utf-8', index=False)
	test_2018_story.to_csv('Dataframes/test_2018_story.csv', encoding='utf-8', index=False)
	test_20181_story.to_csv('Dataframes/test_20181_story.csv', encoding='utf-8', index=False)


def main():
	# Disable chained assignments
	pd.options.mode.chained_assignment = None 

	train_2016, test_2016, test_2018, test_20181 = read_files()

	# Split train set in train and test set for testing purposes
	train, test = train_test_split(train_2016, test_size=0.2, random_state=10)

	# For every file, separate one row into two rows with different endings and create csv files 
	train_sep = separate(train)
	test_sep = separate(test)
	test_2016_sep = separate(test_2016)
	test_2018_sep = separate(test_2018)
	test_20181_sep = separate(test_20181)

	filelist1 = ['Dataframes/train_sep.csv', 'Dataframes/test_sep.csv', 'Dataframes/test_2016_sep.csv', 'Dataframes/test_2018_sep.csv', 'Dataframes/test_20181_sep.csv']

	while True:
		listsep = []

		for file in filelist1:
			listsep.append(os.path.isfile(file))

		if all(listsep):
			print("All seperated files exist")
			break
		else:
			train_sep.to_csv(filelist1[0], encoding='utf-8', index=False)
			test_sep.to_csv(filelist1[1], encoding='utf-8', index=False)
			test_2016_sep.to_csv(filelist1[2], encoding='utf-8', index=False)
			test_2018_sep.to_csv(filelist1[3], encoding='utf-8', index=False)
			test_20181_sep.to_csv(filelist1[4], encoding='utf-8', index=False)			
			print("all separated files created")

	### Stylistic features
	# Create csv files with all the stylistic features
	stylistic_features(train_sep, test_sep, test_2016_sep, test_2018_sep, test_20181_sep)

	### Similarity features based on word embeddings
	# Get dataframes with all the similarity features
	vectors(train_sep, test_sep, test_2016_sep, test_2018_sep, test_20181_sep)
	similarity()

	### BERT
	# Create csv files with just the story
	create_file_bert(train_sep, test_sep, test_2016_sep, test_2018_sep, test_20181_sep)


if __name__ == '__main__':
	main()