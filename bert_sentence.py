from bert_serving.client import BertClient
import pandas as pd


def read_files():
	"""Read CSV files"""
	train_story = pd.read_csv("Dataframes/train_story.csv", delimiter=",")
	test_story = pd.read_csv("Dataframes/test_story.csv", delimiter=",")
	test_2016_story = pd.read_csv("Dataframes/test_2016_story.csv", delimiter=",")
	test_2018_story = pd.read_csv("Dataframes/test_2018_story.csv", delimiter=",")
	test_20181_story = pd.read_csv("Dataframes/test_20181_story.csv", delimiter=",")


	return train_story, test_story, test_2016_story, test_2018_story, test_20181_story


def main():
	train_story, test_story, test_2016_story, test_2018_story, test_20181_story = read_files()

	bc2  = BertClient()

	print("Encode train...")
	train_story['Story'] = train_story['Story'].apply(lambda x: [x]).apply(bc2.encode)
	print("Encode test...")
	test_story['Story'] = test_story['Story'].apply(lambda x: [x]).apply(bc2.encode)
	print("Encode test2016...")
	test_2016_story['Story'] = test_2016_story['Story'].apply(lambda x: [x]).apply(bc2.encode)
	print("Encode test2018...")
	test_2018_story['Story'] = test_2018_story['Story'].apply(lambda x: [x]).apply(bc2.encode)
	print("Encode test20181...")
	test_20181_story['Story'] = test_20181_story['Story'].apply(lambda x: [x]).apply(bc2.encode)

	# Create CSV files
	train_story.to_csv('Dataframes/train_BERT.csv', encoding='utf-8', index=False)
	test_story.to_csv('Dataframes/test_BERT.csv', encoding='utf-8', index=False)
	test_2016_story.to_csv('Dataframes/test_2016_BERT.csv', encoding='utf-8', index=False)
	test_2018_story.to_csv('Dataframes/test_2018_BERT.csv', encoding='utf-8', index=False)
	test_20181_story.to_csv('Dataframes/test_20181_BERT.csv', encoding='utf-8', index=False)


if __name__ == '__main__':
	main()
