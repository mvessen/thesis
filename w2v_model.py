#!/usr/bin/python3

import pandas as pd
from gensim.models import Word2Vec
from gensim.utils import simple_preprocess

# The 2 datasets combine to almost 100,000 stories

ROC2016 = pd.read_csv("Data/ROCStories_spring2016.csv", delimiter=",")
ROC2017 = pd.read_csv("Data/ROCStories_winter2017.csv", delimiter=",")

# Combine 2 dataframes in 1 dataframe
ROCStories = ROC2016.append(ROC2017, ignore_index = True)

# Combine all sentences in one column
ROCStories['stories'] = ROCStories['sentence1'] + " " + ROCStories['sentence2'] + " " + ROCStories['sentence3'] + " " + ROCStories['sentence4'] + " " + ROCStories['sentence5']

ROCStories['stories'] = ROCStories['stories'].apply(simple_preprocess)

stories = ROCStories['stories']

model = Word2Vec(stories)

model.save('word2vec')