#!/usr/bin/python3

import re
import numpy as np
import pandas as pd

from ast import literal_eval
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report, accuracy_score, confusion_matrix


def read_files():
	"""Gets all training and test files as dataframes"""
	# Separated files
	train_sep = pd.read_csv("Dataframes/train_sep.csv", delimiter=",")
	test_sep = pd.read_csv("Dataframes/test_sep.csv", delimiter=",")
	test_2016_sep = pd.read_csv("Dataframes/test_2016_sep.csv", delimiter=",")
	test_2018_sep = pd.read_csv("Dataframes/test_2018_sep.csv", delimiter=",")
	test_20181_sep = pd.read_csv("Dataframes/test_20181_sep.csv", delimiter=",")

	# Stylistic features
	train_styl = pd.read_csv("Dataframes/train_styl.csv", delimiter=",")
	test_styl = pd.read_csv("Dataframes/test_styl.csv", delimiter=",")
	test_2016_styl = pd.read_csv("Dataframes/test_2016_styl.csv", delimiter=",")
	test_2018_styl = pd.read_csv("Dataframes/test_2018_styl.csv", delimiter=",")
	test_20181_styl = pd.read_csv("Dataframes/test_20181_styl.csv", delimiter=",")

	# Similarity features
	train_sim = pd.read_csv("Dataframes/train_cos.csv", delimiter=",")
	test_sim = pd.read_csv("Dataframes/test_cos.csv", delimiter=",")
	test_2016_sim = pd.read_csv("Dataframes/test_2016_cos.csv", delimiter=",")
	test_2018_sim = pd.read_csv("Dataframes/test_2018_cos.csv", delimiter=",")
	test_20181_sim = pd.read_csv("Dataframes/test_20181_cos.csv", delimiter=",")

	# BERT features 
	train_BERT = pd.read_csv("Dataframes/train_BERT.csv", delimiter=",")
	test_BERT = pd.read_csv("Dataframes/test_BERT.csv", delimiter=",")
	test_2016_BERT = pd.read_csv("Dataframes/test_2016_BERT.csv", delimiter=",")
	test_2018_BERT = pd.read_csv("Dataframes/test_2018_BERT.csv", delimiter=",")
	test_20181_BERT = pd.read_csv("Dataframes/test_20181_BERT.csv", delimiter=",")

	return train_sep, test_sep, test_2016_sep, test_2018_sep, test_20181_sep,\
	train_styl, test_styl, test_2016_styl, test_2018_styl, test_20181_styl,\
	train_sim, test_sim, test_2016_sim, test_2018_sim, test_20181_sim,\
	train_BERT, test_BERT, test_2016_BERT, test_2018_BERT, test_20181_BERT


def classifier(cf, X_train, y_train, X_test, y_test):
	"""Create the stylistic classifier"""
	cf.fit(X_train, y_train) # ADD FEATURE SELECTION!!!!
	pred = cf.predict(X_test)
	print(classification_report(y_test, pred))
	tn, fp, fn, tp = confusion_matrix(y_test, pred).ravel()
	print(tn, fp, fn, tp)
	print("Accuracy score: {}\n".format(accuracy_score(y_test, pred)))


def clean_up(i):
	"""Turn column of values in lists in lists into column of values"""
	i = literal_eval(i)

	return float(i[0][0])


def clean_up2(i):
	"""Change string to list of list of values to list of values"""
	i = literal_eval(i)

	return(i[0])


def main():
	# Disable chained assignments
	pd.options.mode.chained_assignment = None 

	train_sep, test_sep, test_2016_sep, test_2018_sep, test_20181_sep,\
	train_styl, test_styl, test_2016_styl, test_2018_styl, test_20181_styl,\
	train_sim, test_sim, test_2016_sim, test_2018_sim, test_20181_sim,\
	train_BERT, test_BERT, test_2016_BERT, test_2018_BERT, test_20181_BERT = read_files()

	# Clean up the cosine similarity column
	train_sim = train_sim['CosSim'].apply(clean_up)
	test_sim = test_sim['CosSim'].apply(clean_up)
	test_2016_sim = test_2016_sim['CosSim'].apply(clean_up)
	test_2018_sim = test_2018_sim['CosSim'].apply(clean_up)
	test_20181_sim = test_20181_sim['CosSim'].apply(clean_up)

	# Create combination dataframe
	train_combo = train_styl.join(train_sim)
	test_combo = test_styl.join(test_sim)
	test_2016_combo = test_2016_styl.join(test_2016_sim)
	test_2018_combo = test_2018_styl.join(test_2018_sim)
	test_20181_combo = test_20181_styl.join(test_20181_sim)

	# Clean up BERT features and split into separate columns
	train_BERT = train_BERT['Story'].apply(clean_up2).to_frame()
	train_bft = pd.DataFrame(train_BERT['Story'].values.tolist())
	test_BERT = test_BERT['Story'].apply(clean_up2).to_frame()
	test_bft = pd.DataFrame(test_BERT['Story'].values.tolist())
	test_2016_BERT = test_2016_BERT['Story'].apply(clean_up2).to_frame()
	test_2016_bft = pd.DataFrame(test_2016_BERT['Story'].values.tolist())
	test_2018_BERT = test_2018_BERT['Story'].apply(clean_up2).to_frame()
	test_2018_bft = pd.DataFrame(test_2018_BERT['Story'].values.tolist())
	test_20181_BERT = test_20181_BERT['Story'].apply(clean_up2).to_frame()
	test_20181_bft = pd.DataFrame(test_20181_BERT['Story'].values.tolist())

	# Create final combination dataframe
	train_combo2 = train_combo.join(train_bft)
	test_combo2 = test_combo.join(test_bft)
	test_2016_combo2 = test_2016_combo.join(test_2016_bft)
	test_2018_combo2 = test_2018_combo.join(test_2018_bft)
	test_20181_combo2 = test_20181_combo.join(test_20181_bft)

	print(train_combo2.shape, test_2016_combo2.shape, test_20181_combo2.shape)

	# Create stylistic classifier
	cf = LogisticRegression()

	# Scores of the classification model with just stylistic features
	print("Stylistic features results")
	pred_test_styl = classifier(cf, train_styl, train_sep['AnswerThisEnding'],
		test_styl, test_sep['AnswerThisEnding'])
	pred_test_2016_styl = classifier(cf, train_styl, train_sep['AnswerThisEnding'],
		test_2016_styl, test_2016_sep['AnswerThisEnding'])
	pred_test_2018_styl = classifier(cf, train_styl, train_sep['AnswerThisEnding'],
		test_2018_styl, test_2018_sep['AnswerThisEnding'])
	pred_test_20181_styl = classifier(cf, train_styl, train_sep['AnswerThisEnding'],
		test_20181_styl, test_20181_sep['AnswerThisEnding'])

	# Scores of the classification model with just similarity features
	print("Similarity features results")
	pred_test_sim = classifier(cf, train_sim.to_frame(), train_sep['AnswerThisEnding'],
		test_sim.to_frame(), test_sep['AnswerThisEnding'])
	pred_test_2016_sim = classifier(cf, train_sim.to_frame(), train_sep['AnswerThisEnding'],
		test_2016_sim.to_frame(), test_2016_sep['AnswerThisEnding'])
	pred_test_2018_sim = classifier(cf, train_sim.to_frame(), train_sep['AnswerThisEnding'],
		test_2018_sim.to_frame(), test_2018_sep['AnswerThisEnding'])
	pred_test_20181_sim = classifier(cf, train_sim.to_frame(), train_sep['AnswerThisEnding'],
		test_20181_sim.to_frame(), test_20181_sep['AnswerThisEnding'])

	#Scores of the classification model with just BERT features
	print("BERT features results")
	pred_test_BERT = classifier(cf, train_bft, train_sep['AnswerThisEnding'],
		test_bft, test_sep['AnswerThisEnding'])
	pred_test_2016_BERT = classifier(cf, train_bft, train_sep['AnswerThisEnding'],
		test_2016_bft, test_2016_sep['AnswerThisEnding'])
	pred_test_2018_BERT = classifier(cf, train_bft, train_sep['AnswerThisEnding'],
		test_2018_bft, test_2018_sep['AnswerThisEnding'])
	pred_test_20181_BERT = classifier(cf, train_bft, train_sep['AnswerThisEnding'],
		test_20181_bft, test_20181_sep['AnswerThisEnding'])

	# Scores of the classification model with all the features except BERT
	print("\n\nAll features results ex BERT")
	pred_test_combo = classifier(cf, train_combo, train_sep['AnswerThisEnding'],
		test_combo, test_sep['AnswerThisEnding'])
	pred_test_2016_combo = classifier(cf, train_combo, train_sep['AnswerThisEnding'],
		test_2016_combo, test_2016_sep['AnswerThisEnding'])
	pred_test_2018_combo = classifier(cf, train_combo, train_sep['AnswerThisEnding'],
		test_2018_combo, test_2018_sep['AnswerThisEnding'])
	pred_test_20181_combo = classifier(cf, train_combo, train_sep['AnswerThisEnding'],
		test_20181_combo, test_20181_sep['AnswerThisEnding'])

	# Scores of the classification model with all the features
	print("\n\nAll features results including BERT")
	pred_test_combo2 = classifier(cf, train_combo2, train_sep['AnswerThisEnding'],
		test_combo2, test_sep['AnswerThisEnding'])
	pred_test_2016_combo2 = classifier(cf, train_combo2, train_sep['AnswerThisEnding'],
		test_2016_combo2, test_2016_sep['AnswerThisEnding'])
	pred_test_2018_combo2 = classifier(cf, train_combo2, train_sep['AnswerThisEnding'],
		test_2018_combo2, test_2018_sep['AnswerThisEnding'])
	pred_test_20181_combo2 = classifier(cf, train_combo2, train_sep['AnswerThisEnding'],
		test_20181_combo2, test_20181_sep['AnswerThisEnding'])


if __name__ == '__main__':
	main()