# Thesis

This repository contains all the code that I have written and used for my thesis. It consists of 2 classifiers, one using stylistic, similarity and language model features and the other using BERT. 