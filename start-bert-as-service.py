#!/usr/bin/python3

# start-bert-as-service.py -pooling_layer -2 -model_dir uncased_L-12_H-768_A-12/ -max_seq_len=50

import sys

from bert_serving.server import BertServer
from bert_serving.server.helper import get_run_args


if __name__ == '__main__':
    args = get_run_args()
    server = BertServer(args)
    server.start()
    server.join()